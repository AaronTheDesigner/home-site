import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import DashHandler from './DashHandler';
import UserProfile from '../UserProfile';

const Dash = () => {

    const [view, setView] = useState('main');
    const permissions = useSelector(state => state.permissions.role)
    console.log(permissions)

    const checkRole = () => {
        if (permissions !== 'contributor') {
            return (
                <div>
                    <p>Permission not granted.</p>
                </div>
            )
        }

        return (
            <div>
                <DashHandler />
                <UserProfile />
            </div>
        )
        
    }


    return (
        <div>
            {checkRole()}
        </div>
    )
}

export default Dash

