import Axios from 'axios';

//Axios.defaults.baseURL = 'http://67.207.83.201/api/v1';
Axios.defaults.baseURL = 'http://localhost:5000/api/v1';
Axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('auth');
Axios.defaults.headers.post['Content-Type'] = 'application/json';


// AUTH \\
//public\\
export const signUp = (name, email, password) => {
    return Axios.post('/auth/register', {
        name,
        email,
        password
    })
}

export const login = (email, password) => {
    return Axios.post(`/auth/login`, {
        email,
        password
    })
}

//private\\

export const userDetails = () => {
    return Axios.get('auth/me')
}

export const logout = () => {
    return Axios.get('/auth/logout')
}

// USERS \\
//public\\
export const getUsers = () => {
    return Axios.get('/users')
}

//private\\
export const getUser = (id) => {
    return Axios.get(`/users/${ id }`)
}


export const createUser = (name, email, password, role) => {
    return Axios.post(`/users`), {
        name,
        email,
        password,
        role
    }
}

export const deleteUser = (id) => {
    return Axios.delete(`/users/${id}`)
}

export const updateUser = (id, name, email, role) => {
    return Axios.put(`/users/${id}`), {
        name,
        email,
        role
    }
}


// ARTICLES \\
//public\\

export const getArticles = () => {
    return Axios.get('articles')
}

export const getArticlePhoto = (id) => Axios.create({
    url: `/uploads/photo_${ id }.jpg`,
    method: 'get'

})

export const getArticle = (id) => {
    return Axios.get(`/articles/${ id }`)
}

//private\\
export const createArticle = ({ title, subtitle, description, content, tag, pub }) => {
    return Axios.post('/articles', {
        title,
        subtitle,
        description,
        content,
        tag,
        pub
    })
}

export const uploadPhoto = (id, file) => {
    return Axios.put(`/articles/${ id }/photo`, {
        file
    })
}

export const updateArticle = (id, subtitle, description, content, tag, pub) => {
    return Axios.put(`/articles/${ id }`, {
        subtitle,
        description,
        content,
        tag,
        pub
    })
}


export const deleteArticle = (id) => {
    return Axios.delete(`/articles/${ id }`)
}

// MULTI \\
//public\\
export const getMulti = () => {
    Axios.all([
        Axios({
            url: '/articles',
            method: 'get'
        }),
        Axios({
            url: '/users',
            method: 'get'
        })
    ]).then(res => {
        console.log(res[0].data.data);
        console.log(res[1].data.data);
    }).catch(err => console.log(err))
}