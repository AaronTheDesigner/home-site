import React from 'react';

const UserProfile = (props) => {


    return (
        <div className="profile-container">
            <p>{props.id}</p>
            <p>{props.name}</p>
            <p>{props.role}</p>
            <p>{props.email}</p>
            <p>{props.createdAt}</p>
        </div>
    )
}

export default UserProfile
