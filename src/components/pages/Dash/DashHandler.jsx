import React from 'react';
import { Link } from 'react-router-dom'

const DashHandler = () => {
    return (
        <ul>
            <li>
                <Link to="/myarticles">My Articles</Link>
            </li>
        </ul>
    )
}

export default DashHandler
