import React from 'react';
import IconScroll from '../../icons/IconScroll';

const Header = () => {
    return (
        <div>
            <header className="section-header" id="section-header">
                <div className="logo">
                    <div className="logo logo__center">
                        <svg className="logo logo__center--main" width="239" height="240" viewBox="0 0 239 240" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path className="logo logo__center--main--1" stroke="white" d="M3.72461 0H235.493L210.661 39.3179H28.557L3.72461 0Z" fill="#AB5050" />
                            <path className="logo logo__center--main--2" stroke="white" d="M84.0327 159.764L119.195 89.7367L154.06 159.764H198.758L119.195 0.638184L0 239.029H44.1023L64.3655 199.098L84.0327 159.764ZM218.426 199.396H174.323L193.99 239.029H238.391L218.426 199.396Z" fill="#BE9F7D" />
                            <path className="logo logo__center--main--3" stroke="white" d="M174.323 199.396H218.426L238.391 239.029H193.99L174.323 199.396Z" fill="#BE9F7D" />
                            <path className="logo logo__center--main--4" stroke="white" d="M174.323 199.396H218.426L238.391 239.029H193.99L174.323 199.396Z" fill="#77A7B6" />
                        </svg>
                    </div>
                    <div className="logo__name">
                        <h2 className="animate-name"><span className="color-brown-primary animate-letter">A</span>aron  <span className="color-red-primary animate-letter">T</span>oliver</h2>
                    </div>
                    <div className="logo__pro">
                        <h4 className="color-blue-primary animate-full" >Web Developer</h4>
                    </div>
                </div>

                <p className="section-header__about">
                    I'm a web developer based in Charlotte, NC. Skilled in both <span className="highlight color-blue-primary">Front</span> and <span className="highlight color-red-primary">Back</span> End services. I provide digital solutions for small businesses, through <span className="highlight">transparency</span> and <span className="highlight">education</span>.
                </p>

                <div className="section-header__scroll">
                    <a href="#section-services" className="scroll-link">
                        <IconScroll />
                    </a>
                </div>


            </header>
        </div>
    )

}

export default Header
