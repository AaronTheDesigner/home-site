import React from 'react';
import IconFront from '../../icons/IconFront';
import IconBack from '../../icons/IconBack';
import IconFull from '../../icons/IconFull';
import IconJS from '../../icons/IconJS';
import IconContact from '../../icons/IconContact';

function Skills() {
    return (
        <section className="skills" id="section-skills">
            <div className="skills__container">
                <div className="skills__header" data-aos="fade-down">
                    <h1 className="color-blue-primary skills--title">Skills</h1>
                    <p>My belief in life long education pushes me to constantly learn new things.</p>
                </div>
                <span className="skill" data-aos="fade-right">
                    <div className="skill-cover">
                        <div className="skill-icon">
                            <IconFront />
                        </div>
                        <div className="skill-label">
                            <p className="color-blue-primary">Front End</p>
                        </div>
                    </div>
                    <div className="skill-reveal">
                        <ul className="skill-list color-blue-primary">
                            <li className="skill-list__item"><p>HTML5</p></li>
                            <li className="skill-list__item"><p>CSS3</p></li>
                            <li className="skill-list__item"><p>Sass</p></li>
                            <li className="skill-list__item"><p>Javascript</p></li>
                            <li className="skill-list__item"><p>ReactJs</p></li>
                            <li className="skill-list__item"><p>Responsive Design</p></li>
                            <li className="skill-list__item"><p>Version Control / Git</p></li>
                            <li className="skill-list__item"><p>Browser Developer Tools</p></li>
                            <li className="skill-list__item"><p>A/B Testing</p></li>
                            <li className="skill-list__item"><p>Unit Testing</p></li>
                        </ul>
                    </div>
                </span>
            </div>

            <div className="skills__container">
                <span className="skill" data-aos="fade-left">
                    <div className="skill-reveal">
                        <ul className="skill-list color-red-primary">
                            <li className="skill-list__item"><p>MongoDB</p></li>
                            <li className="skill-list__item"><p>MySQL</p></li>
                            <li className="skill-list__item"><p>Postgress</p></li>
                            <li className="skill-list__item"><p>NodeJS</p></li>
                            <li className="skill-list__item"><p>Express</p></li>
                            <li className="skill-list__item"><p>REST API</p></li>
                            <li className="skill-list__item"><p>Graph API</p></li>
                            <li className="skill-list__item"><p>MVC Architecture</p></li>
                            <li className="skill-list__item"><p>Postman API Documentation</p></li>
                            <li className="skill-list__item"><p>Structural Testing</p></li>
                            <li className="skill-list__item"><p>Functional Testing</p></li>
                        </ul>
                    </div>
                    <div className="skill-cover">
                        <div className="skill-icon">
                            <IconBack />
                        </div>
                        <div className="skill-label">
                            <p className="color-red-primary">Back End</p>
                        </div>
                    </div>
                </span>
            </div>

            <div className="skills__container">
                <span className="skill" data-aos="fade-right">
                    <div className="skill-cover">
                        <div className="skill-icon">
                            <IconFull />
                        </div>
                        <div className="skill-label">
                            <p className="color-brown-primary">FullStack</p>
                        </div>
                    </div>
                    <div className="skill-reveal">
                        <ul className="skill-list color-brown-primary">
                            <li className="skill-list__item"><p>CI</p></li>
                            <li className="skill-list__item"><p>CD</p></li>
                            <li className="skill-list__item"><p>AWS</p></li>
                            <li className="skill-list__item"><p>Heroku</p></li>
                            <li className="skill-list__item"><p>Digital Ocean</p></li>
                            <li className="skill-list__item"><p>Nginx</p></li>
                            <li className="skill-list__item"><p>Lambda</p></li>
                            <li className="skill-list__item"><p>MERN Stack</p></li>
                            <li className="skill-list__item"><p>Microservices</p></li>
                        </ul>
                    </div>
                </span>
            </div>

            <div className="skills__container">
                <span className="skill" data-aos="fade-left">
                    <div className="skill-reveal">
                        <ul className="skill-list ">
                            <li className="skill-list__item"><p>ES6</p></li>
                            <li className="skill-list__item"><p>DOM Manipulation</p></li>
                            <li className="skill-list__item"><p>Object Oriented Javascript</p></li>
                            <li className="skill-list__item"><p>Asynchronous Javascript</p></li>
                            <li className="skill-list__item"><p>Fetch API / Axios</p></li>
                            <li className="skill-list__item"><p>Design Patterns</p></li>
                            <li className="skill-list__item"><p>Error Handling</p></li>
                            <li className="skill-list__item"><p>Regular Expressions</p></li>
                        </ul>
                    </div>
                    <div className="skill-cover">
                        <div className="skill-icon">
                            <IconJS />
                        </div>
                        <div className="skill-label">
                            <p className="">Javascript</p>
                        </div>
                    </div>
                </span>
            </div>
            <div className="cta-section">
                <div className="services__cta btn-container">
                    <a href="#section-contact" className="btn btn-link">
                        <div className="cta-slider">
                            <span className="cta-icon"><IconContact /></span>
                            <span className="cta-name"><p>Contact</p></span>
                        </div>
                    </a>
                </div>
            </div>
        </section>
    )
}

export default Skills
