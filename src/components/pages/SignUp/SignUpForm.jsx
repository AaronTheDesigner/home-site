import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { signUp } from '../../axios';
import { Redirect } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setLogin, setRole } from '../../../redux/actions'

const SignUpForm = () => {
    const { handleSubmit, register, errors } = useForm();
    const [complete, setComplete] = useState(false)

    const storage = window.localStorage;
    const dispatch = useDispatch();

    const handleSignUp = values => {
        signUp(values.name, values.email, values.password)
            .then(res => {
                storage.setItem('auth', res.data.token);
                dispatch(setLogin({ loggedIn: true }))
                dispatch(setRole({ role: 'user' }))
                setComplete(true)
            })
            .catch(err => {
                const error = err.response.data.error;
                console.log(error);
            })
    }

    const renderRedirect = () => {
        if (complete === true) {
            return <Redirect to='/' />
        }

        return <h1>Sign Up</h1>
    }

    return (
        <div>
            {renderRedirect()}
            <form action="" onSubmit={handleSubmit(handleSignUp)}>
                <input
                    type="text"
                    placeholder="Place Your Name Here"
                    name="name"
                    ref={register({ required: "Required Field" })}
                />
                <input
                    type="email"
                    placeholder="Enter Email Here"
                    name="email"
                    ref={register({ required: "Required Field" })}
                />
                <input
                    type="password"
                    placeholder="Enter Password Here"
                    name="password"
                    ref={register({
                        required: "Required Field",
                        minLength: { value: 6, message: "Minimum Length: 6 Characters" }
                    })}
                />
                {errors.password && errors.password.message}
                {errors.email && errors.email.message}
                <input type="submit" value="Sign Up" />
            </form>

        </div>
    )
}

export default SignUpForm


// {complete === true ? <Redirect to={{
//     pathname: `/articles/${ articleSlug }`,
//     state: { id: articleId }
// }} /> : "Submit Photo"}