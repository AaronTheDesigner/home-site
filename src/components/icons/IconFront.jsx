import React from 'react'

function IconFront() {
    return (
        <svg className="icon-front" width="100" height="76" viewBox="0 0 100 76" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path className="icon-front-peice" d="M0 37.9465V76H12.6212V50.7575H50.5799V37.9465H12.6212H0ZM0 0.0827625L12.6212 12.7989H50.5799V0.0827625H0ZM49.3017 74.9561V76H99.8816V63.2839H61.923L49.3017 74.9561ZM49.3017 0.0827625L61.923 12.7989H99.8816V0.0827625H49.3017ZM49.3017 31.7782V44.3994H61.923H99.8816V31.7782H61.923H49.3017Z" fill="#5F8895" />
        </svg>
    )
}

export default IconFront
