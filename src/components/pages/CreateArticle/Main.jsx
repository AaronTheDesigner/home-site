import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import CreateArticle from './CreateArticle';

const Main = () => {
    const [view, setView] = useState('main');
    const permissions = useSelector(state => state.permissions.role)

    const checkRole = () => {
        if (permissions !== 'admin' || permissions !== 'contributor') {
            return <CreateArticle />
        }

        return "Permission not granted..."
    }

    return (
        <div>
            {checkRole()}
        </div>
    )
}

export default Main

