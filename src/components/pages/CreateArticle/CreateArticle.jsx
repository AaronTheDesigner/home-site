import React, { useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { createArticle } from '../../axios';
import UploadPhoto from './UploadPhoto'
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Redirect } from 'react-router-dom'


const CreateArticle = (params, props) => {
    const [articleId, setArticleId] = useState('');
    const [articleSlug, setArticleSlug] = useState('');
    const { control, handleSubmit, register } = useForm();
    const [complete, setComplete] = useState(false);
    const [ckData, setCkData] = useState(null)

    const onSubmit = data => {
        // place tags into an array
        modifyTag(data);
        // retrive data from CKEditor, place into 'content'
        modifyContent(data)
        // imported create article (POST) from axios.js
        createArticle(data)
            .then(res => {
                setArticleId(res.data.data.id);
                setArticleSlug(res.data.data.slug)
            })
            .catch(err => console.log(err.response.data.error))
    }

    const modifyContent = (data) => {
        // React Hook Form troubles with CK and handleSubmit
        // manually replace content with ckData
        data.content = ckData
    }

    const modifyTag = data => {
        let tagArray = [];
        tagArray.push(data.tagOne, data.tagTwo)
        data.tag = tagArray;
        delete data.tagOne;
        delete data.tagTwo;
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)} >
                <input type="text" placeholder="Title" name="title" ref={register({ maxLength: 80 })} />
                <input type="text" placeholder="Subtitle" name="subtitle" ref={register} />
                <textarea name="description" cols="30" rows="15" ref={register} />
                <Controller
                    as={<CKEditor
                        editor={ClassicEditor}
                        onBlur={(event, editor) => {
                            //save data to content variable
                            const content = editor.getData();
                            //save content to CkData hook
                            setCkData(content)
                        }}
                    />}
                    name="content" control={control}
                />
                <select name="tagOne" ref={register}>
                    <option value="Web Development">Web Development</option>
                    <option value="Mobile Development"> Mobile Development</option>
                    <option value="Tutorial"> Tutorial</option>
                    <option value="Diary"> Diary</option>
                    <option value="Web Design"> Web Design</option>
                    <option value="UI/UX"> UI/UX</option>
                </select>
                <select name="tagTwo" ref={register}>
                    <option value="Web Development">Web Development</option>
                    <option value="Mobile Development"> Mobile Development</option>
                    <option value="Tutorial"> Tutorial</option>
                    <option value="Diary"> Diary</option>
                    <option value="Web Design"> Web Design</option>
                    <option value="UI/UX"> UI/UX</option>
                </select>
                <input type="checkbox" placeholder="public" name="public" ref={register} />

                <input type="submit" />
            </form>
            {articleId !== '' ? <UploadPhoto
                id={articleId}
                toggleComp={setComplete} /> : "Complete Form"}
            <br />
            {complete === true ? <Redirect to={{
                pathname: `/articles/${ articleSlug }`,
                state: { id: articleId }
            }} /> : "Submit Photo"}
        </div>
    )
}

export default CreateArticle;

//<textarea name="content" id="editor" cols="30" rows="15" ref={register} ></textarea>