import React from 'react';
import SignUpForm from './SignUpForm';

const index = () => {
    return (
        <div>
            <SignUpForm />
        </div>
    )
}

export default index
