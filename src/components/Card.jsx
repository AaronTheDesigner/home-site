import React from 'react'

const Card = (props) => {

    return (
        <div className="card-container">
            <img src={props.image} alt={props.title} />
            <h5 className="card-title">
                {props.title}
            </h5>
            <div className="card-content">
                <h6 className="card-subtitle">
                    {props.subtitle}
                </h6>
                <p className="card-description">{props.description}</p>
            </div>
        </div>
    )
}

export default Card
