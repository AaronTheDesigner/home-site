import React from 'react';
import { Link } from 'react-router-dom';

const AdminNav = () => {
    return (
        <div>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/blog">Blog</Link>
                </li>
                <li>
                    <Link to="/me">Profile</Link>
                </li>
                <li>
                    <Link to="/admin" >Admin</Link>
                </li>
            </ul>
        </div>
    )
}

export default AdminNav