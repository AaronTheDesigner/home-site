import React from 'react';
//import './App.css';
import Nav from './components/Nav/index.jsx'
import Home from './components/pages/Home';
import Blog from './components/pages/Blog';
import UserProfile from './components/pages/UserProfile';
import ProjectTracker from './components/pages/ProjectTracker';
import Dash from './components/pages/Dash';
import MyArticleContainer from './components/pages/Dash/MyArticleContainer';
import Admin from './components/pages/Admin';
import ArticleView from './components/pages/Article/ArticleView';
import Personal from './components/pages/Personal';
import Professional from './components/pages/Professional';
import SignUp from './components/pages/SignUp';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

function App() {

  return (
    <div>
      <Router>
        <Nav />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/blog" component={Blog} />
          <Route path="/me" component={UserProfile} />
          <Route path="/project" component={ProjectTracker} />
          <Route path="/dash" component={Dash} />
          <Route path="/admin" exact component={Admin} />
          <Route path='/articles/:slug' component={ArticleView} />
          <Route path='/projects/personal' exact component={Personal} />
          <Route path='/projects/professional' exact component={Professional} />
          <Route path='/signup' exact component={SignUp} />
          <Route path='/myarticles' component={MyArticleContainer} />
        </Switch>
      </Router>
    </div>
  );

}

export default App;

// protect myarticles, me, project
