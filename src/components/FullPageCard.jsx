import React from 'react'

const FullPageCard = (props) => {

    const createMarkup = () => {
        return { __html: props.content }
    }

    const returnContent = () => {
        return <div dangerouslySetInnerHTML={createMarkup()} />;
    }

    return (
        <div className="article-container">
            <img src={props.image} alt={props.title} className="article-pic" />
            <h1 className="article-title">{props.title}</h1>
            <h6 className="article subtitle">{props.subtitle}</h6>
            {returnContent()}
            <p className="article-date">{props.date}</p>
        </div>
    )
}

export default FullPageCard

