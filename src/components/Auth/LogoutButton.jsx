import React from 'react';
import { logout } from '../axios';
import { useDispatch } from 'react-redux';
import { setLogin, setRole } from '../../redux/actions';

const LogoutButton = () => {
    const dispatch = useDispatch();
    let storage = window.localStorage;

    const handleLogout = () => {
        logout().then(res => {
            storage.removeItem('auth');
            storage.removeItem('randid');
            dispatch(setLogin({ loggedIn: false }))
            dispatch(setRole({ role: 'guest' }))
        })
    }
    return (
        <div>
            <input type="button" value="Logout" onClick={handleLogout} />
        </div>
    )
}

export default LogoutButton
