import React from 'react';
import AnalyticsContainer from './containers/AnalyticsContainer';
import ArticlesContainer from './containers/ArticlesContainer';
import UsersContainer from './containers/UsersContainer';
import CreateArticle from '../CreateArticle';
import HomePageContainer from './containers/HomePageContainer';

const MainView = (props) => {

    const renderView = () => {
        return props.view === 'articles' ? <ArticlesContainer />
            : props.view === 'users' ? <UsersContainer />
                : props.view === 'create' ? <CreateArticle />
                    : props.view === 'home' ? <HomePageContainer />
                        : <AnalyticsContainer />
    }

    return (
        <div>
            {renderView()}
        </div>
    )
}

export default MainView
