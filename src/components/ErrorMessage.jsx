import React from 'react'

const ErrorMessage = (props) => {
    return (
        <div>
            <p>{props.message}</p>
        </div>
    )
}

export default ErrorMessage
