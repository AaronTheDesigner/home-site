import React from 'react'
import { Editor } from '@tinymce/tinymce-react';

const MCEditor = (props) => {

    const handleEditorChange = (content, editor) => {
        console.log('Content was updated:', content);
    }

    console.log(props.name)
    return (
        <Editor
            apiKey='nys5pge3fkem3lto8lbwvjsfgvdqvw0n3l0pjw4iqmhnyxxl'
            name={props.name}
            init={{

                menubar: false,
                width: 300,
                height: 300,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar:
                    'undo redo | formatselect | bold italic backcolor | \
             alignleft aligncenter alignright alignjustify code | \
             bullist numlist outdent indent | removeformat | help'
            }}
            onEditorChange={handleEditorChange()}
        />
    )
}

export default MCEditor
