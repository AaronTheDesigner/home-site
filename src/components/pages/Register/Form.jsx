import React from 'react'
import { signUp } from '../../axios';
import { useForm } from "react-hook-form";

const Form = () => {
    const { handleSubmit, register, errors } = useForm();
    const onSubmit = values => {
        signUp(values.name, values.email, values.password)
    };

    return (
        <div>
            <form action="" onSubmit={handleSubmit(onSubmit)}>
                <input
                    type="name"
                    placeholder="name"
                    name="name"
                    ref={register({ required: "Required Field" })} />
                <input
                    type="email"
                    placeholder="email"
                    name="email"
                    ref={register({ required: "Required Field" })} />
                <input
                    type="password"
                    placeholder="password"
                    name="password"
                    ref={register({ required: "Required Field", minLength: { value: 6, message: "Minimum Length: 6 characters" } })} />

                {errors.password && errors.password.message}
                {errors.email && errors.email.message}
                {errors.name && errors.name.message}
                <input type="submit" />
            </form>
        </div>
    )
}

export default Form


