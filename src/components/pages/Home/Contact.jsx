import React from 'react';
import IconMail from '../../icons/IconMail';
import IconLinkedIn from '../../icons/IconLinkedIn';

const Contact = () => {

    return (
        <section className="contact" id="section-contact">
            <div className="contact__header" data-aos="fade-down">
                <h1 className="color-red-primary contact--title" >Contact</h1>
                <p >Feel free to contact me through any of the methods below. My availability varies based on the services requested.</p>
            </div>
            <div className="contact__info">
                <a className="contact-item" href="mailto: aaronaugustine85@gmail.com"><IconMail /></a>
                <a className="contact-item" href="https://www.linkedin.com/in/aaron-toliver-14820262/"><IconLinkedIn /></a>
            </div>
        </section>
    )

}

export default Contact
