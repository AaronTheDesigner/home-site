import React, { useEffect, useState } from 'react';
import { getArticles } from '../../../axios';
import Card from '../../../Card';
import EditArticle from '../../../EditArticle';

const ArticlesContainer = () => {
    const [articles, setArticles] = useState([]);
    const [view, setView] = useState('');
    const [edit, setEdit] = useState();

    useEffect(() => {
        fetchGallery();
    }, [])

    const fetchGallery = () => {
        getArticles().then(res => {
            const articles = res.data.data;
            setArticles(articles)
        })
    }

    const renderGallery = articles.map(article => {

        return (
            <div
                key={article.id}
                onClick={() => {
                    setEdit(article)
                    setView('article')
                }}
            >
                <Card
                    title={article.title}
                    description={article.description}
                />
            </div>
        )
    })

    const renderView = () => {
        return view === 'article' ? <EditArticle
            id={edit.id}
            view={() => setView()}
        />
            : renderGallery
    }

    return (
        <div>
            {renderView()}
        </div>
    )
}

export default ArticlesContainer
