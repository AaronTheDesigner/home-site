import React from 'react';
import IconPWA from '../../icons/IconPWA';
import IconContact from '../../icons/IconContact';
import IconDEV from '../../icons/IconDEV';
import IconDES from '../../icons/IconDES';
import IconCMS from '../../icons/IconCMS';
import IconAPI from '../../icons/IconAPI';
import IconCardReveal from '../../icons/IconCardReveal';


const Services = () => {

    return (
        <section className="services" >
            <div className="services__header" data-aos="fade-down">
                <h1 className="service--title" id="section-services">Services</h1>
                <p className="service--description">All of my services are listed. But my greatest strength is my personalized approach. I have more than a decade of customer service experience. And I've got a talent for discovering your needs and meeting them. Customers receive exclusive access to my client portal, with a daily progress tracker, bug reports, and more.</p>

            </div>

            <div className="services__main">
                <div className="main-card__container" data-aos="fade-left">
                    <div className="main-card__icon">
                        <IconPWA />
                    </div>
                    <p className="main-card__title">Progressive Web Applications</p>
                    <p className="main-card__description">
                        The culmination of all services provided. A progressive web application provides the <span className="highlight">exposure</span> of a website and the <span className="highlight">engagement</span> of a mobile application from one source. All without the costly fees of the App Store or Google Play. Enjoy the best of both worlds at minimum cost.
                    </p>
                </div>
            </div>

            <div className="services__cards">

                <div className="service-card service-card--1" data-aos="fade-right">
                    <div className="service-card__container">
                        <div className="service-card__slider">
                            <div className="service-card__cover"  >
                                <span className="service-card__icon">
                                    <IconDEV />
                                </span>
                                <span className="service-card__label">
                                    <p className="service-card__title">Web Development</p>
                                </span>
                                <span className="service-card__down"  >
                                    <IconCardReveal />
                                </span>
                            </div>
                            <div className="service-card__reveal">
                                <p className="service-card__description">
                                    A website is a valuable marketing tool for both startups and well-established brands. I place thought in to every stage of a website to make sure your online presence is <span className="highlight">on brand</span> and achieves your goals.
                                </p>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="service-card service-card--2" data-aos="fade-left">
                    <div className="service-card__container">
                        <div className="service-card__slider">
                            <div className="service-card__cover">
                                <span className="service-card__icon">
                                    <IconDES />
                                </span>
                                <span className="service-card__label">
                                    <p className="service-card__title">Web Design</p>
                                </span>
                                <span className="service-card__down">
                                    <IconCardReveal />
                                </span>
                            </div>
                            <div className="service-card__reveal">
                                <p className="service-card__description">
                                    You and your business are unique. And you deserve an online identity that is <span className="highlight">distinct</span> . With UI/UX design, and Wireframes, I help revamp an existing brand, or help create a new one.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="service-card service-card--3" data-aos="fade-right">
                    <div className="service-card__container">
                        <div className="service-card__slider">
                            <div className="service-card__cover">
                                <span className="service-card__icon">
                                    <IconCMS />
                                </span>
                                <span className="service-card__label">
                                    <p className="service-card__title">Content Management Systems</p>
                                </span>
                                <span className="service-card__down">
                                    <IconCardReveal />
                                </span>
                            </div>
                            <div className="service-card__reveal">
                                <p className="service-card__description">
                                    <span className="highlight">Content is King</span>. When it comes to shaping that content, you know what's best. Go beyond cookie cutter services like Wordpress, Squarespace, and Drupal. And never pay a developer again to update a paragraph.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="service-card service-card--4" data-aos="fade-left">
                    <div className="service-card__container">
                        <div className="service-card__slider">
                            <div className="service-card__cover">
                                <span className="service-card__icon">
                                    <IconAPI />
                                </span>
                                <span className="service-card__label">
                                    <p className="service-card__title">API Integration</p>
                                </span>
                                <span className="service-card__down">
                                    <IconCardReveal />
                                </span>
                            </div>
                            <div className="service-card__reveal">
                                <p className="service-card__description">
                                    There are many online platforms that can enhance your visibility. By integrating them into your website, you can save valuable time. <span className="highlight">Superpower</span> your online presence with public API's like Google Maps, Twitter, Facebook, and LinkedIn.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div className="services__cta">
                <a href="#section-contact" className="btn btn-link">
                    <div className="cta-slider">
                        <span className="cta-icon"><IconContact /></span>
                        <span className="cta-name"><p>Contact</p></span>
                    </div>
                </a>
            </div>
        </section>
    )
}

export default Services
