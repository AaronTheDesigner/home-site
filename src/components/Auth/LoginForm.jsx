import React from 'react';
import { login } from '../axios';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { setLogin } from '../../redux/actions';


const LoginForm = () => {
    const { handleSubmit, register, errors } = useForm();

    const storage = window.localStorage;
    const dispatch = useDispatch();

    const handleLogin = values => {
        login(values.email, values.password)
            .then(res => {
                storage.setItem('auth', res.data.token);
                dispatch(setLogin({ loggedIn: true }))
            })
            .catch(err => {
                const error = err.response.data.error;
                console.log(error)
            })

    }

    return (
        <div>
            <form action="" onSubmit={handleSubmit(handleLogin)}>
                <input
                    type="email"
                    placeholder="Enter Email Here"
                    name="email"
                    ref={register({ required: "Required Field" })}
                />
                <input
                    type="password"
                    placeholder="Enter Password Here"
                    name="password"
                    ref={register({
                        required: "Required Field",
                        minLength: { value: 6, message: "Minimum Length: 6 Characters" }
                    })}
                />
                {errors.password && errors.password.message}
                {errors.email && errors.email.message}
                <input type="submit" value="Login" />
            </form>
        </div>
    )
}

export default LoginForm
