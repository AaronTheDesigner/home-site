import React, {useState, useEffect} from 'react';
import {getUsers} from '../../../axios';
import Card from '../../../Card';
import EditUser from '../../../EditUser';

const UsersContainer = () => {
    const [users, setUsers] = useState([]);
    const [view, setView] = useState('');
    const [edit, setEdit] = useState();

    useEffect(() => {
        fetchUsers();
    }, [])


    const fetchUsers = () => {
        return getUsers().then(res => {
            const users = res.data.data;
            setUsers(users);
        })
    }

    const renderUsers = users.map(user => {
        return (
            <div
            key={user._id}
            onClick={() => {
                setEdit(user)
                setView('user')
            }}
            >

                
                    name={user.name}
                    email={user.email}
                    role={user.role}
            </div>
        )
    })

    const renderView = () => {
        return view === 'user' ? <EditUser 
            id={edit._id}
            view={() => setView()}
        />
            : renderUsers
    } 
 
    return (
        <div>
            {renderView()}
        </div>
    )
}

export default UsersContainer
