import React from 'react';
import SideNode from '../../icons/SideNode';
import SideUp from '../../icons/SideUp';
import SideDown from '../../icons/SideDown';


export default function SideNav() {
    return (
        <ul className="side-nav">
            <li className="side-nav__item">
                <a href="#section-header" className="side-nav__item--link" alt="Up">
                    <SideUp className="side-nav__item--icon" />
                </a>
            </li>
            <li className="side-nav__item">
                <a href="#section-header" className="side-nav__item--link" alt="Main">
                    <SideNode />
                </a>
            </li>
            <li className="side-nav__item">
                <a href="#section-services" className="side-nav__item--link" alt="Services">
                    <SideNode />
                </a>
            </li>
            <li className="side-nav__item">
                <a href="#section-skills" className="side-nav__item--link" alt="Works">
                    <SideNode />
                </a>
            </li>
            <li className="side-nav__item">
                <a href="#section-contact" className="side-nav__item--link" alt="Blog">
                    <SideNode />
                </a>
            </li>
            <li className="side-nav__item">
                <a href="#section-contact" className="side-nav__item--link" alt="Down">
                    <SideDown />
                </a>
            </li>
        </ul>
    )
}
