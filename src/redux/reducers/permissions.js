import { SET_ROLE } from '../actions/actionTypes';

const initialState = {
    role: "guest"
}

export const permissions = (state = initialState, action) => {
    switch (action.type) {
        case SET_ROLE:
            return { ...state, ...state.role = action.str }
        default:
            return state
    }
}