import React, { useState } from 'react';
import { useSelector } from 'react-redux'
import SideBar from './SideBar';
import MainView from './MainView';

const Admin = () => {
    const [view, setView] = useState('main')
    const permissions = useSelector(state => state.permissions.role)

    const checkRole = () => {
        return permissions === 'admin' ? renderAdmin()
            : "Permission not granted..."
    }

    const renderAdmin = () => {
        return (
            <div>
                <SideBar state={{ view: [view, setView] }} />
                <MainView view={view} />
            </div>
        )
    }

    return (
        <div>
            {checkRole()}
        </div>
    )


}

export default Admin
