import React, { useState, useEffect } from 'react';
// import { useParams } from 'react-router-dom'
import FullPageCard from '../../FullPageCard';
import UserProfile from '../../UserProfile';
import axios from 'axios';

const ArticleView = (props) => {
    const [articleId] = useState(props.location.state.id);
    const [article, setArticle] = useState({});
    const [author, setAuthor] = useState({});
    // let { slug } = useParams();

    useEffect(() => {
        fetchArticle();
    }, []);

    const fetchArticle = async () => {
        axios.get(`/articles/${ articleId }`)
            .then(res => {
                const article = res.data.data;
                setArticle(article)
                fetchAuthor(article.user);
            })

    }

    const fetchAuthor = (id) => {
        axios.get(`/users/${ id }`)
            .then(res => {
                const author = res.data.data;
                setAuthor(author)
            })
    }

    return (
        <div>
            <FullPageCard
                image={`http://localhost:5000/uploads/${ article.photo }`}
                title={article.title}
                subtitle={article.subtitle}
                content={article.content}
                date={article.createdAt}
            />
            <UserProfile
                name={author.name}
                role={author.role}
                email={author.email}
            />
        </div>
    )
}

export default ArticleView
