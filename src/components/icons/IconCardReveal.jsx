import React from 'react'

function IconCardReveal() {
    return (
        <svg className="icon-card-reveal" width="77" height="77" viewBox="0 0 77 77" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path className="icon-card-reveal-peice" d="M27.5057 25.9207L38.7034 48.2214L49.8063 25.9207H64.0408L38.7034 76.5955L0.744827 0.678223H14.7895L21.2425 13.3944L27.5057 25.9207ZM70.304 13.2995H56.2593L62.5225 0.678223H76.6621L70.304 13.2995Z" fill="black"/>
        </svg>
    )
}

export default IconCardReveal
