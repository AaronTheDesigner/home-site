import React, { useState } from 'react';
import Axios from 'axios';

const UploadPhoto = (props, params) => {
    const [file, setFile] = useState(null);
    const [filename, setFilename] = useState(null);

    const onSubmit = e => {
        e.preventDefault();
        let formData = new FormData();
        formData.append('file', file);
        formData.append('name', filename);
        formData.append('type', file.type)

        // Display the key/value pairs
        Axios.put(`/articles/${ props.id }/photo`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            props.toggleComp(true)
        }).catch(err => console.log(err.response.data.error))

    }

    const handleChange = (e) => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);

    }

    return (
        <div>
            <form onSubmit={onSubmit}>
                <h2>Cover Image</h2>
                <label>
                    Select A File:
            </label>
                <input
                    type="file"
                    name="file"
                    onChange={handleChange}
                />
                <input type="submit" accept="image/png, image/jpeg" />
            </form>
        </div>
    )
}

export default UploadPhoto;