import React, { useState, useEffect } from 'react';
import { getArticles } from '../../axios';
import Card from '../../Card';
import { Link } from 'react-router-dom';


const Gallery = props => {
    const [articles, setArticles] = useState([]);

    useEffect(() => {
        fetchArticles();
    }, [])

    const fetchArticles = () => {
        getArticles().then(res => {
            const articleList = res.data.data;
            setArticles(articleList)
        })
    }
    
    const articleCard = articles.map(article => {
        return (
            <Link key={article.id} to={{
                pathname: `/articles/${ article.slug }`,
                state: { id: article.id }

            }}>
                <Card
                    title={article.title}
                    subtitle={article.subtitle}
                    description={article.description}
                    image={`http://localhost:5000/uploads/${ article.photo }`}
                />
            </Link>
        )

    })

    return (
        <div>
            {articleCard}
        </div>
    )

}

export default Gallery
